<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class castEl extends Model
{
    protected $table = 'cast';
    protected $fillable = ['nama','umur','bio'];
}
