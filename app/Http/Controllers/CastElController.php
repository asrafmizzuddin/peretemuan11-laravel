<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\castEl;
class CastElController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $castel = castEl::all();
        
        return view('castEl.index', compact('castel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listcast = DB::table('cast')->get();
        return view ('castEl.create', compact('listcast'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio'  => 'required',
        ]);

        $castel = new castEl;

        $castel->nama = $request->nama;
        $castel->umur = $request->umur;
        $castel->bio = $request->bio;

        $castel->save();

        return redirect('castEl');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $castel = castEl::findOrFail($id);
        return view('castEl.show', compact('castel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $listcast = DB::table('cast')->get();
        $castel = castEl::findOrFail($id);
        return view('castEl.edit', compact('listcast','castel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio'  => 'required',
        ]);
        $castel = castEl::find($id);

        $castel->nama = $request->nama;
        $castel->umur = $request->umur;
        $castel->bio = $request->bio;

        $castel->save();

        return redirect('castEl');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $castel = castEl::find($id);

        $castel->delete();

        return redirect('castEl');
    }
}
