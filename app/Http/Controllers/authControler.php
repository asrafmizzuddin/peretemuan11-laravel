<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class authControler extends Controller
{
    public function form (){ 
        return view('halaman.form');
    }
    public function welcome (Request $request){ 
        $fname = $request['fname'];
        $lname = $request['lname'];
        return view('halaman.welcome', compact('fname','lname'));
    }
}
