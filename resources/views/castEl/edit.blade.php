@extends('layout.master')

@section('judul')
Edit Cast  {{$castel->nama}}

@endsection

@section('content')
<h3>Form Edit Cast</h3>
<form action="/castEl/{{$castel->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama</label>
        <input type="text" name="nama" value="{{$castel->nama}}" class="form-control" aria-describedby="nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="integer" name="umur" value="{{$castel->umur}}"  class="form-control" aria-describedby="umur">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
        <label>Bio</label>
        <textarea type="text" name="bio" class="form-control" rows="10">{{$castel->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

  @endsection