@extends('layout.master')

@section('judul')
Detail Cast {{$castel->nama}}

@endsection

@section('content')
<h3>{{$castel->nama}}</h3>
<p>{{$castel->bio}}</p>
<a href="/castEl" class="btn btn-secondary">Back</a>
@endsection