@extends('layout.master')

@section('judul')
Tabel Cast

@endsection

@section('content')

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No.</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Aksi</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($listcast as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td>
                    <form action="/cast/{{$item->id}}" method="POST">
                        <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm mt-1 mb-1">Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm mt-1 mb-1">Edit</a>
                        @method('delete')
                        @csrf
                        <input type="submit" class="btn btn-danger btn-sm" value="Hapus">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>
                    Data masih kosong
                </td>
            </tr>
        @endforelse
    </tbody>
  </table>
  <a href="/cast/create" class="btn btn-success mt-3">Tambah Data</a>
@endsection