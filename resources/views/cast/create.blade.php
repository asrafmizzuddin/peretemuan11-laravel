@extends('layout.master')

@section('judul')
Input Cast

@endsection

@section('content')
<h3>Form Cast</h3>
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label>Nama</label>
        <input type="text" name="nama"class="form-control" aria-describedby="nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="integer" name="umur" class="form-control" aria-describedby="umur">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
        <label>Bio</label>
        <textarea type="text" name="bio" class="form-control" rows="10"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

  @endsection