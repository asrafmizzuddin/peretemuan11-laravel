@extends('layout.master')

@section('judul')
Buat Account Baru!

@endsection

@section('content')
<h3>Sign Up Form</h3>
<form action="/welcome" method="POST">
    @csrf
    <label for="fname">First name:<br></label><br>
    <input type="text" id="fname" name="fname" ><br><br>
    <label for="lname">Last name:<br></label><br>
    <input type="text" id="lname" name="lname" ><br><br>
    <p>Gender:</p>
    <input type="radio" id="male" name="gender" value="male">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="gender" value="female">
    <label for="css">Female</label><br>
    <input type="radio" id="other" name="gender" value="other">
    <label for="other">Other</label><br><br>
    <label for="warga_negara">Nationality:</label><br><br>
    <select id="wn" name="wn">
        <option value="indo">Indonesia</option>
        <option value="amerika">Amerika</option>
        <option value="inggris">Inggris</option>
        <option value="foreign">Foreign</option>
    </select>
    <br>
    <p>Language Spoken:</p>
    <input type="checkbox" id="bhs1" name="bhs" value="indo">
    <label for="vehicle1"> Bahasa Indonesia</label><br>
    <input type="checkbox" id="bhs2" name="bhs" value="english">
    <label for="vehicle2"> English</label><br>
    <input type="checkbox" id="bhs3" name="bhs" value="others">
    <label for="vehicle3"> Other</label><br><br>
    <p>Bio:</p>
    <textarea name="message" rows="10" cols="30" placeholder="Isi bio kamu ya!"></textarea>
    <br>
    <input type="submit" value="Sign Up">
  </form>
</body>
@endsection

